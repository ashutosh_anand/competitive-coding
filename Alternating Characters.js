/*
  Problem Statement --> "Shashank likes strings in which consecutive characters are different.
  For example, he likes ABABA, while he doesn't like ABAA.
  Given a string containing characters AA and BB only, he wants to change it into a string he likes.
  To do this, he is allowed to delete the characters in the string."

  Sample Input -->
  5
  AAAA
  BBBBB
  ABABABAB
  BABABA
  AAABBB

  Sample Output -->
  3
  4
  0
  0
  4
*/


function processData(input) {
    //Enter your code here
    var inpArr = input.split("\n");

    var times = parseInt(inpArr[0]);

    for(var i=1;i<=times;i++) {
      // Get the string and save the first character
      var string = inpArr[i];
      var curr_char = string[0];
      var deletions = 0;

      for(var x=1;x<string.length;x++) {
        if (curr_char === string[x]) {
          deletions ++;
        } else {
          curr_char = string[x];
        }
      }
      console.log(deletions);
    }
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});
