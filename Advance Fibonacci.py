def main():
    inp = input()
    a = int(inp[0])
    b = int(inp[1])
    c = int(inp[2]) - 2

    while(c > 0) :
        n = nextNumber(a, b)
        a = b
        b = n

    print(n)

def nextNumber(a, b) :
    return a + b ** 2

main()
